var app = new Vue({
  el: '#app',
  data: {
    news: [],
    // newsPerPage: [],
    sources: [],
    api: 'https://newsapi.org/v1/articles?source=${source}&sortBy=top&apiKey=aa5abb9102574808a6a19eb679fe1c0c',
    sourcesApi: 'https://newsapi.org/v1/sources',
    loaded: false,
    loading: false,
    selectedSource: '',
    standartSource: 'reddit-r-all',
    // sortby: true // true - top, false - new
  },
  created: function () {
    this.loadSources()
    this.loadNews()
  },
  methods: {
    loadSources: function () {
      const xmlHttp = new XMLHttpRequest();
      xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState === 4 && xmlHttp.status === 200) {
          processResponse(xmlHttp.responseText);
        }
      };
      xmlHttp.open('GET', this.sourcesApi, true);
      xmlHttp.send(null);
      let self = this
      function processResponse(response) {

        const responseObj = JSON.parse(response);
        self.sources = responseObj.sources
      }
    },
    loadNews: function (source, sortby) {
      this.news.forEach(function(element) {
        element.urlToImage = ''
      }, this);
      const xmlHttp = new XMLHttpRequest();
      xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState === 4 && xmlHttp.status === 200) {
          processResponse(xmlHttp.responseText);
        }
      };
      source = source ? source : this.standartSource
      xmlHttp.open('GET', this.api.replace('${source}', source), true);
      xmlHttp.send(null);
      let self = this
      function processResponse(response) {


        const responseObj = JSON.parse(response);
        responseObj.articles.forEach(function (element, index) {
          // if(index < 2){
          //   element.show = true
          // }
          // console.log('sd')
          let objDate = new Date(element.publishedAt),
            locale = "en-us",
            month = objDate.toLocaleString(locale, { month: "long" });

          date = new Date(element.publishedAt)
          element.date = month + ' ' + date.getDate() + ', ' + date.getFullYear()
          self.loaded = true
        }, this);
        self.news = responseObj.articles
      }

    },changeNews: function(event){
      this.loadNews(event.target.value)
      
    }
  
  }
})